package guildwar.map;

import guildwar.beings.Race;

/**
 * Define a point in the map
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class Point {

    /**
     * The x value of the point
     */
    private int x;
    /**
     * The y value of the point
     */
    private int y;
    /**
     * The map where the point is linked to
     */
    public Map map;
    /**
     * The content of the point
     */
    private Content content;
    /**
     * Tell if the point is in a safeZone
     */
    private boolean isSafeZone;
    /**
     * If the point is in a safeZone. This variable contain the safeZone
     */
    private SafeZone safeZone;


    /**
     * The constructor
     *
     * @param map The map where the point is linked to
     * @param x The x value of the point
     * @param y The y value of the point
     */
    public Point(Map map, int x, int y) {
        this.x = x;
        this.y = y;
        this.map = map;
        this.isSafeZone = false;
        this.content = new Content();
    }

    /**
     * Tell if the point is in a safeZone
     *
     * @return the value of the attribute {@link #isSafeZone
     */
    public boolean isSafeZone() {
        return this.isSafeZone;
    }

    /**
     * Getter for the attribute {@link #x}
     *
     * @return {@link #x}
     */
    public int getX() {
        return this.x;
    }

    /**
     * Getter for the attribute {@link #y}
     *
     * @return {@link #y}
     */
    public int getY() {
        return this.y;
    }

    /**
     * Getter for the attribute {@link #content}
     *
     * @return {@link #content}
     */
    public Content getContent() {
        return this.content;
    }


    /**
     * Set the point as a point of a safeZone
     * @param map The map
     * @param race The race to which belongs the safeZone
     * @param safeZone The safeZone to which the point is linked
     *
     */
    public void setAsSafeZone(Map map, Race race, SafeZone safeZone) {
        this.isSafeZone = true;
        this.safeZone = safeZone;
    }

    /**
     * Getter for the attribute {@link #safeZone}
     *
     * @return {@link #safeZone}
     */
    public SafeZone getSafeZone() {
        return this.safeZone;
    }

    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }
}

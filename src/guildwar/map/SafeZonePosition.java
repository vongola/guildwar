package guildwar.map;


/**
 * This class define the position of a safeZone
 * <ul>
 * <li>{@link #startWidth}</li>
 * <li>{@link #startHeight}</li>
 * <li>{@link #endWidth}</li>
 * <li>{@link #endHeight}</li>
 * </ul>
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class SafeZonePosition {

    /**
     * The position of the safeZone
     */
    private EnumSafeZonePosition safeZonePosition;
    /**
     * The x coordinate of the end of the safeZone
     */
    private int endWidth;
    /**
     * The x coordinate of the start of the safeZone
     */
    private int startWidth;
    /**
     * The y coordinate of the start of the safeZone
     */
    private int startHeight;
    /**
     * The y coordinate of the end of the safeZone
     */
    private int endHeight;
    /**
     * The map
     */
    private Map map;

    /**
     * The constructor of the class
     *
     * @param map The map where the safeZone is linked
     * @param safeZonePosition The position of the safeZone
     */
    public SafeZonePosition(Map map, EnumSafeZonePosition safeZonePosition) {
        this.map = map;
        this.safeZonePosition = safeZonePosition;
        if (safeZonePosition == EnumSafeZonePosition.NORTHWEST) {
            this.startWidth = 0;
            this.startHeight = 0;
            this.endWidth = map.getWidth() / 4;
            this.endHeight = map.getHeight() / 4;
        }
        else if (safeZonePosition == EnumSafeZonePosition.NORTHEAST) {
            this.startWidth = 3 * map.getWidth() / 4;
            this.startHeight = 0;
            this.endWidth = map.getWidth();
            this.endHeight = map.getHeight() / 4;
        }
        else if (safeZonePosition == EnumSafeZonePosition.SOUTHWEST) {
            this.startWidth = 0;
            this.startHeight = 3 * map.getHeight() / 4;
            this.endWidth = map.getWidth() / 4;
            this.endHeight = map.getHeight();
        }
        else if (safeZonePosition == EnumSafeZonePosition.SOUTHEAST) {
            this.startWidth = 3 * map.getWidth() / 4;
            this.startHeight = 3 * map.getHeight() / 4;;
            this.endWidth = map.getWidth();
            this.endHeight = map.getHeight();
        }
    }

    /**
     * Getter of the attribute {@link #startHeight}
     * @return {@link #startHeight}
     */
    public int getStartHeight() {
        return this.startHeight;
    }

    /**
     * Getter of the attribute {@link #startWidth}
     * @return {@link #startWidth}
     */
    public int getStartWidth() {
        return this.startWidth;
    }

    /**
     * Getter of the attribute {@link #endHeight}
     * @return {@link #endHeight}
     */
    public int getEndHeight() {
        return this.endHeight;
    }

    /**
     * Getter of the attribute {@link #endWidth}
     * @return {@link #endWidth}
     */
    public int getEndWidth() {
        return this.endWidth;
    }

    /**
     * Getter of the attribute {@link #safeZonePosition}
     * @return {@link #safeZonePosition}
     */
    public EnumSafeZonePosition getSafeZonePosition() {
        return this.safeZonePosition;
    }

    /**
     * Getter of the attribute {@link #map}
     * @return {@link #map}
     */
    public Map getMap() { return this.map; }
}

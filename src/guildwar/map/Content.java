package guildwar.map;

import guildwar.beings.Being;
import guildwar.map.Obstacle;

/**
 * Represent the content of a {@link Point}
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class Content {

    /**
     * The content is an obstacle if this attribute is not null
     */
    private Obstacle obstacle = null;
    /**
     * The content is a being is this attribute is not null
     */
    private Being being = null;

    /**
     * The constructor. create a null content
     *
     */
    public Content() {}

    /**
     * The constructor. Create a content with a new object inside
     *
     * @param cont The content
     */
    public Content(Object cont) {

        if (cont instanceof Being) {
            this.being = (Being) cont;
        }
        else if (cont instanceof Obstacle) {
            this.obstacle = (Obstacle) cont;
        }
        else {
            System.out.println("Error: Trying to instantiate the class content but "
            		+ "didn't provide an object of class Being or Obstacle");
        }
    }

    /**
     * Get the content
     *
     * @return null if there is no content
     * @return {@link #obstacle} if the content is an obstacle
     * @return {@link #being} if the content is a being
     */
    public Object getValue() {
        if (this.obstacle != null) {
            return this.obstacle;
        }
        else if (this.being != null) {
            return this.being;
        }
        else {
            return null;
        }
    }

    /**
     * Set the value of the content
     *
     * @param content The new value of the content
     */
    public void setValue(Object content) {
    	if (content instanceof Being) {
    	    this.being = (Being)content;
    	    this.obstacle = null;
        }
    	else if (content instanceof Obstacle) {
    	    this.obstacle = (Obstacle)content;
    	    this.being = null;
        }
    	else {
    	    this.obstacle = null;
    	    this.being = null;
        }
    }

    /**
     * Set the value of the content to null
     */
    public void setValueToNull() {
    	this.being = null;
    	this.obstacle = null;
    }
}

package guildwar.map;

/**
 * Define the different directions
 * <ul>
 * <li>{@link #NORTHEAST}</li>
 * <li>{@link #NORTHWEST}</li>
 * <li>{@link #SOUTHEAST}</li>
 * <li>{@link #SOUTHWEST}</li>
 * <li>{@link #SOUTH}</li>
 * <li>{@link #WEST}</li>
 * <li>{@link #NORTH}</li>
 * <li>{@link #EAST}</li>
 * </ul>
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public enum Direction {

    /**
     * East
     */
    EAST("East"),
    /**
     * West
     */
    WEST("West"),
    /**
     * North
     */
    NORTH("North"),
    /**
     * South
     */
    SOUTH("South"),
    /**
     * North west
     */
    NORTHWEST("North west"),
    /**
     * North east
     */
    NORTHEAST("North east"),
    /**
     * South east
     */
    SOUTHEAST("South east"),
    /**
     * South west
     */
    SOUTHWEST("South west");

    private String direction;

    private Direction(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return this.direction;
    }
}

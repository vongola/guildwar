package guildwar.map;

/**
 * Define an obstacle
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class Obstacle {

    /**
     * The type of obstacle
     */
    private String type;
    /**
     * The currentPoint of the obstacle
     */
    private Point currentPoint;
    /**
     * The mark for the obstacles
     */
    public static char mark;

    /**
     * The constructor
     *
     * @param currentPoint The {@link Point} of the obstacle
     * @param type The type of obstacle
     */
    public Obstacle(Point currentPoint, String type) {
        this.currentPoint = currentPoint;
        this.type = type;
    }

    /**
     * The getter for the attribute {@link #type}
     *
     * @return {@link #type}
     */
    public String getType() {
        return this.type;
    }

    /**
     * The getter for the attribute {@link #mark}
     *
     * @return {@link #mark}
     */
    public char getMark() {
    	return Obstacle.mark;
    }

    /**
     * The setter for the attribute {@link #mark}
     *
     */
    public static void setMark(char newMark) { Obstacle.mark = newMark; }

    /**
     * The getter for the attribute {@link #currentPoint}
     *
     * @return {@link #currentPoint}
     */
    public Point getCurrentPoint() {
        return this.currentPoint;
    }

    /**
     * The setter for the attribute {@link #currentPoint}
     *
     */
    public void setCurrentPoint(Point newPoint) {
        this.currentPoint = newPoint;
    }
}

package guildwar.map;

/**
 * Abstract class Mapping
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public abstract class Mapping {
	
    protected int width;
    protected int height;
    protected Point[][] pointMapping;   //    a mapping of all the positions in the map (obstacles, beings). If there is nothing in the position, the value is null
    
    public abstract int getWidth();
    public abstract int getHeight();
}

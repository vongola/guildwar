package guildwar.map;


import guildwar.beings.Race;

/**
 * Define a safeZone
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class SafeZone {

    /**
     * The race to which belongs the safeZone
     */
    protected Race race;
    /**
     * The map where the safeZone is linked
     */
    protected Map map;
    /**
     * The position of the safeZone
     */
    private SafeZonePosition safeZonePosition;

    /**
     * The constructor
     *
     * @param map The map where the safeZone is linked
     * @param race The race to which belongs the safeZone
     * @param safeZonePosition The position of the safeZone
     */
    public SafeZone(Map map, Race race, SafeZonePosition safeZonePosition) {
        this.map = map;
        this.race = race;
        this.safeZonePosition = safeZonePosition;
        this.setAsSafeZone();
    }

    /**
     * The getter for the attribute {@link #race}
     *
     * @return {@link #race}
     */
    public Race getRace() {
        return this.race;
    }

    /**
     * The getter for the attribute {@link #map}
     *
     * @return {@link #map}
     */
    public Map getMap() { return this.map; }

    /**
     * The getter for the attribute {@link #safeZonePosition}
     *
     * @return {@link #safeZonePosition}
     */
    public SafeZonePosition getSafeZonePosition() {
        return this.safeZonePosition;
    }

    /**
     * Set all the point of the safeZone by changing the value of the boolean attribute isSafeZone of the {@link Point} to true
     */
    private void setAsSafeZone() {
        for (int i = this.safeZonePosition.getStartWidth() ; i < this.safeZonePosition.getEndWidth(); i++) {
            for (int j = this.safeZonePosition.getStartHeight(); j < this.safeZonePosition.getEndHeight(); j++) {
                this.map.getPointMapping()[i][j].setAsSafeZone(this.map, Race.HUMAN, this);
            }
        }
    }
}

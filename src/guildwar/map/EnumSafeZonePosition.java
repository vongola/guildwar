package guildwar.map;

/**
 * Define the different position of a safeZone
 * <ul>
 * <li>{@link #NORTHEAST}</li>
 * <li>{@link #NORTHWEST}</li>
 * <li>{@link #SOUTHEAST}</li>
 * <li>{@link #SOUTHWEST}</li>
 * </ul>
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
enum EnumSafeZonePosition {

    /**
     * North west
     */
    NORTHWEST("North west"),
    /**
     * North east
     */
    NORTHEAST("North east"),
    /**
     * South east
     */
    SOUTHEAST("South east"),
    /**
     * South west
     */
    SOUTHWEST("South west");

    private String safeZonePosition;

    private EnumSafeZonePosition(String safeZonePosition) {
        this.safeZonePosition = safeZonePosition;
    }

    @Override
    public String toString() {
        return this.safeZonePosition;
    }
}

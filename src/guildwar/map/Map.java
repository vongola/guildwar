package guildwar.map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import guildwar.Game;
import guildwar.beings.Race;
import guildwar.beings.Type;
import guildwar.beings.evil.Goblin;
import guildwar.beings.evil.Orc;
import guildwar.beings.kind.Elf;

import java.util.Random;

import guildwar.beings.Being;
import guildwar.beings.kind.Human;

/**
 * This class represent the map of the game
 *
 */
public class Map extends Mapping {

	/**
	 * The width of the map
	 */
    private int width;
	/**
	 * The height of the map
	 */
	private int height;
	/**
	 * A list of all the safeZones
	 */
    List<SafeZone> safeZones = new ArrayList<>(4);
	/**
	 * a mapping of all the positions in the map (obstacles, beings). If there is nothing in the position, the value is null
	 */
	private Point[][] pointMapping;
	/**
	 * an object of class {@link Random}
	 */
    private static Random rand = new Random();
	/**
	 * A list of all the beings in the map
	 */
	private ArrayList<Being> beings;

	private ArrayList<Integer> namesPositionArray;

	ArrayList<ArrayList<Integer>> nFactorial;

	int loopW;

	int loopH;

    public Map(int width, int height, ArrayList<Integer> namesPositionArray) {
        this.width = width;
        this.height = height;
        this.namesPositionArray = namesPositionArray;
        this.pointMapping = new Point[width][height];
        for (int i = 0; i < pointMapping.length; i++) {
			for (int j = 0; j < pointMapping[i].length; j++) {
				pointMapping[i][j] = new Point(this, i, j);
			}
		}
        this.beings = new ArrayList<Being>(width*height);

        this.nFactorial = new ArrayList<>(width);

        ArrayList<Integer> tmp = new ArrayList<>(height);
		for (int i = 0; i < height; i++) {
			tmp.add(i);
		}
        for (int i = 0; i < width; i++) {
        	this.nFactorial.add((ArrayList<Integer>)tmp.clone());
		}

		this.loopW = width;
		this.loopH = height;

    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }
    
    public void setHeight(int longr) {
    	this.height = longr;
    }
    
    /**
     * Initialize the map: safeZone, obstacles, beings
     */
    public void initialize() throws IOException, InterruptedException {

    	Random generator = new Random();
    	int n = this.width*this.width/100;
    	int k;
    	if (this.width == 50) {
			k = (this.width*this.width - n - 100 + rand.nextInt(20) - 10)/4;
		}
    	else if (this.width == 100) {
			k = (this.width*this.width - n - 6000 + rand.nextInt(100) - 50)/4;
		}
    	else if (this.width == 250) {
			k = (this.width*this.width - n - 10000 + rand.nextInt(1000) - 500)/4;
		}
    	else {
			k = (this.width*this.width - n - 100000 + rand.nextInt(10000) - 5000)/4;
		}


    	// Obstacle
		System.out.println("======== Generating " + (n+10) + " obstacles ...");
		Thread.sleep(1500);
		this.generateObstacles(n+10);

		//SafeZone
		System.out.println("======== Generating safeZones ...");
		Thread.sleep(1500);
		int player = rand.nextInt(4);
		switch(player) {
			case 0:
				SafeZone humanSafeZone = new SafeZone(this, Race.HUMAN, new SafeZonePosition(this, EnumSafeZonePosition.NORTHWEST));
				SafeZone orcSafeZone = new SafeZone(this, Race.ORC, new SafeZonePosition(this, EnumSafeZonePosition.NORTHEAST));
				SafeZone elfSafeZone = new SafeZone(this, Race.ELF, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHWEST));
				SafeZone goblinSafeZone = new SafeZone(this, Race.GOBLIN, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHEAST));

				this.safeZones.add(humanSafeZone);
				this.safeZones.add(orcSafeZone);
				this.safeZones.add(elfSafeZone);
				this.safeZones.add(goblinSafeZone);

				break;
			case 1:
				orcSafeZone = new SafeZone(this, Race.ORC, new SafeZonePosition(this, EnumSafeZonePosition.NORTHWEST));
				humanSafeZone = new SafeZone(this, Race.HUMAN, new SafeZonePosition(this, EnumSafeZonePosition.NORTHEAST));
				elfSafeZone = new SafeZone(this, Race.ELF, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHWEST));
				goblinSafeZone = new SafeZone(this, Race.GOBLIN, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHEAST));

				this.safeZones.add(orcSafeZone);
				this.safeZones.add(humanSafeZone);
				this.safeZones.add(elfSafeZone);
				this.safeZones.add(goblinSafeZone);

				break;
			case 2:
				elfSafeZone = new SafeZone(this, Race.ELF, new SafeZonePosition(this, EnumSafeZonePosition.NORTHWEST));
				orcSafeZone = new SafeZone(this, Race.ORC, new SafeZonePosition(this, EnumSafeZonePosition.NORTHEAST));
				humanSafeZone = new SafeZone(this, Race.HUMAN, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHWEST));
				goblinSafeZone = new SafeZone(this, Race.GOBLIN, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHEAST));

				this.safeZones.add(elfSafeZone);
				this.safeZones.add(orcSafeZone);
				this.safeZones.add(humanSafeZone);
				this.safeZones.add(goblinSafeZone);

				break;
			case 3:
				goblinSafeZone = new SafeZone(this, Race.GOBLIN, new SafeZonePosition(this, EnumSafeZonePosition.NORTHWEST));
				orcSafeZone = new SafeZone(this, Race.ORC, new SafeZonePosition(this, EnumSafeZonePosition.NORTHEAST));
				elfSafeZone = new SafeZone(this, Race.ELF, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHWEST));
				humanSafeZone = new SafeZone(this, Race.HUMAN, new SafeZonePosition(this, EnumSafeZonePosition.SOUTHEAST));

				this.safeZones.add(goblinSafeZone);
				this.safeZones.add(orcSafeZone);
				this.safeZones.add(elfSafeZone);
				this.safeZones.add(humanSafeZone);

				break;
		}

		// Beings
		System.out.println("======== Generating " + k + " beings ...");
		Thread.sleep(1500);
		this.generateBeings(k);

    }

    /**
     * create randomly obstacles and call the method drawObtacles() to draw the obstacles in the map
     */
    public void generateObstacles(int number) {
    	int x, y;
	    for (int i = 0; i < number; i++) {
			x = rand.nextInt(this.loopW);
			Collections.shuffle(this.nFactorial.get(x));
			y = (this.nFactorial.get(x)).get(0);
			this.nFactorial.get(x).remove(0);
			if (this.nFactorial.get(x).size() == 0) {
				this.nFactorial.remove(x);
				this.loopW--;
			}
			pointMapping[x][y].getContent().setValue(new Obstacle(this.pointMapping[x][y], "ob"));
		}
    }

     // generate beings. The variable "number" is the amount of being to create for each species
    public void generateBeings(int number) throws IOException{
		int x, y;
		//HUMAN
		System.out.println("==== Generating " + number + " humans ...");
		Human human;
		for(int i = 0; i< number; i++) {
			x = rand.nextInt(this.loopW);
			Collections.shuffle(this.nFactorial.get(x));
			y = (this.nFactorial.get(x)).get(0);
			this.nFactorial.get(x).remove(0);
			if (this.nFactorial.get(x).size() == 0) {
				this.nFactorial.remove(x);
				this.loopW--;
			}
			human = new Human(this.pointMapping[x][y], readLine(this.namesPositionArray.get(i% Game.NAMES_FILE_NUMBER_OF_LINE), "./human_name.txt"));
			beings.add(human);
			this.pointMapping[x][y].getContent().setValue(human);
		}
		//ELF
		System.out.println("==== Generating " + number + " elfs ...");
		Elf elf;
		for(int i = 0; i< number; i++) {
			x = rand.nextInt(this.loopW);
			Collections.shuffle(this.nFactorial.get(x));
			y = (this.nFactorial.get(x)).get(0);
			this.nFactorial.get(x).remove(0);
			if (this.nFactorial.get(x).size() == 0) {
				this.nFactorial.remove(x);
				this.loopW--;
			}
			elf = new Elf(this.pointMapping[x][y], readLine(this.namesPositionArray.get(i% Game.NAMES_FILE_NUMBER_OF_LINE), "./elf_name.txt"));
			beings.add(elf);
			this.pointMapping[x][y].getContent().setValue(elf);
		}
		//ORC
		System.out.println("==== Generating " + number + " orcs ...");
		Orc orc;
		for(int i = 0; i< number; i++) {
			x = rand.nextInt(this.loopW);
			Collections.shuffle(this.nFactorial.get(x));
			y = (this.nFactorial.get(x)).get(0);
			this.nFactorial.get(x).remove(0);
			if (this.nFactorial.get(x).size() == 0) {
				this.nFactorial.remove(x);
				this.loopW--;
			}
			orc = new Orc(this.pointMapping[x][y], readLine(this.namesPositionArray.get(i% Game.NAMES_FILE_NUMBER_OF_LINE), "./orc_name.txt"));
			beings.add(orc);
			this.pointMapping[x][y].getContent().setValue(orc);
		}
		//GOBLIN
		System.out.println("==== Generating " + number + " goblins ...");
		Goblin goblin;
		for(int i = 0; i< number; i++) {
			x = rand.nextInt(this.loopW);
			Collections.shuffle(this.nFactorial.get(x));
			y = (this.nFactorial.get(x)).get(0);
			this.nFactorial.get(x).remove(0);
			if (this.nFactorial.get(x).size() == 0) {
				this.nFactorial.remove(x);
				this.loopW--;
			}
			goblin = new Goblin(this.pointMapping[x][y], readLine(this.namesPositionArray.get(i% Game.NAMES_FILE_NUMBER_OF_LINE), "./goblin_name.txt"));
			beings.add(goblin);
			this.pointMapping[x][y].getContent().setValue(goblin);
		}
    }  
   /* public void drawBeings() {
    for(Being being : beings) {
    	int x = point.getX();
    	int y = point.getY();
    	Content content = pointMapping[x][y].getContent();
    	if (content == null) {
    		 mark = "";
    	}
    	if(content.getValue() instanceof Being){
    		mark = being.getMark();
    	}else if(content.getValue() instanceof Obstacle){
    		//mark = Obstacle.getMark();
    	}
    }

    }
    public void drawObstacles() {
    	
    }*/
    
     // draw the map in the command line interface
    public void drawMap() throws InterruptedException { //faut-il utiliser les entiers ?
    	System.out.println();
    	System.out.println("------------------------------------------------------");
    	System.out.println("-------------- drawing map ---------------------------");
		System.out.println("------------------------------------------------------");
		System.out.println();
		Thread.sleep(1500);
		System.out.print(" ");
		for (Point[] points : this.pointMapping) {
			for (Point point : points) {
				//System.out.println(this.pointMapping[i][j].getContent().getValue());
				if (point.getContent().getValue() instanceof Being) {
					Being being = (Being) point.getContent().getValue();
					System.out.print(being.getMark());
				} else if (point.getContent().getValue() instanceof Obstacle) {
					Obstacle obstacle = (Obstacle) point.getContent().getValue();
					System.out.print(obstacle.getMark());
				} else {
					System.out.print(" ");
				}
			}
			System.out.print("\n");
		}
    }
   // draw the map in the window
    public void drawGuiMap() {
    	
    } 
     // add the content (characters, obstacles, safezone) to the map
    public void addContentToMap(){
    	
    } 
     // equivalent of addContentToMap() but is in a window
    public void addContentToGuiMap() {
    	
    }

	public ArrayList<Being> getBeings() {
		return this.beings;
	}

	public List<SafeZone> getSafeZones() { return this.safeZones; }

	public Point[][] getPointMapping() { return this.pointMapping; }

	public static String readLine(int line, String fileName) throws IOException {
		String content;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			for (int i = 0; i < line - 1; i++)
				br.readLine();
			content =  br.readLine();
			br.close();
			return content;
		}
	}

}

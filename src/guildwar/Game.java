package guildwar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import guildwar.beings.Being;
import guildwar.beings.Race;
import guildwar.beings.Type;
import guildwar.beings.evil.Evil;
import guildwar.beings.evil.Goblin;
import guildwar.beings.evil.Orc;
import guildwar.beings.kind.Elf;
import guildwar.beings.kind.Human;
import guildwar.beings.kind.Kind;
import guildwar.map.Map;
import guildwar.map.Obstacle;


/**
 * The class define the method {@link Game#start()} to start the game
 * We can only instantiate this class one time
 * @author Lahat DEME
 * @author Wanqi ZHENG
 * @author Junior Tagne
 * @author Brice NICODEM
 */
public class Game {
	/**
	 * The unique instance of the class
	 * Is equal to null when the class has never been instantiated
	 */
	private static Game myGame;
	/**
	 * A logger to display warnings
	 */
	private static final Logger LOGGER = Logger.getLogger( "logger" );
	/**
	 * The max round we can do in this game
	 */
	private static int MAX_ROUND = 10;
	/**
	 * An array that will contain integer from 0 to {@link Game#NAMES_FILE_NUMBER_OF_LINE}
	 */
	private ArrayList<Integer> namesPositionArray;
	/**
	 * The number of line of the name files
	 */
	public static int NAMES_FILE_NUMBER_OF_LINE = 70;
    /**
     * An {@link HashMap} containing the default configuration of the game
     */
    private static HashMap defaultConfig = new HashMap();
	/**
	 * The default size of the side of the map
	 */
	private static int DEFAULT_SIDE = 50;
	/**
	 * The allow value for the size of the side
	 */
	private static String[] ALLOWED_SIDE = { "50", "100", "250", "500"};
	/**
	 * The default mark for the human race
	 */
	private static char DEFAULT_HUMAN_MARK = 'h';
	/**
	 * The default mark for the orc race
	 */
	private static char DEFAULT_ORC_MARK = 'o';
	/**
	 * The default mark for the elf race
	 */
	private static char DEFAULT_ELF_MARK = 'e';
	/**
	 * The default mark for the goblin race
	 */
	private static char DEFAULT_GOBLIN_MARK = 'g';
	/**
	 * The default mark for the obstacles
	 */
	private static char DEFAULT_OBSTACLE_MARK = '#';
	/**
     * An {@link HashMap} representing the current configuration of the game
     */
    private static HashMap currentConfig = new HashMap();
    /**
     * The Map of the game
     */
    private static Map map;

	/**
	 * The constructor of the class Game made private
	 * The class can only be instantiated one time
	 * To instantiate the class {@link Game}, you should call the method {@linkplain Game#getInstance()}
	 * If the method is called a second time, it will return the already created class
	 *
	 */
	private Game() {

		String[] marksArray = new String[5];
		boolean conformConfigFile = true;
		int side = 50; 	// Side of the map ( = width = height)
        namesPositionArray = new ArrayList<>(NAMES_FILE_NUMBER_OF_LINE);
        for (int i = 0; i < NAMES_FILE_NUMBER_OF_LINE; i++) {
            namesPositionArray.add(i);
        }


		/**
		 * The path to the config file
		 */
		String file = "./config.txt";
		
		/*
		 * Recover the config:
		 * The line 1 contains the width and the height of the map (width = height)
		 * The line 2 contains the marks representing each race separated by space
		 * 		- The mark 1 is for the human race
		 * 		- The mark 2 is for the goblin race
		 * 		- The mark 3 is for the elf race
		 * 		- The mark 4 is for the orc race
		 *
		 */
		String value;
		try {
			// Read the width and the height (1rst line)
			value = readLine(1, file);
			if((Integer.parseInt(value) == 50) || (Integer.parseInt(value) == 100) || (Integer.parseInt(value) == 250) || (Integer.parseInt(value) == 500)) {
				side = Integer.parseInt(value);

			}
			else {
				conformConfigFile = false;
			}

			// Read the marks
			value = readLine(2, file);
			marksArray = value.split(" ");
			if (marksArray.length != 5) {
				conformConfigFile = false;
			}
			else {
				for (String mark : marksArray) {
					if (mark == null) {
						conformConfigFile = false;
						break;
					}
				}
			}

		} catch (IOException | NumberFormatException e) {
			e.printStackTrace();
			// Error reading the file; using the default config
			conformConfigFile = false;
		} // Problem reading the value in the file, exiting

		if (conformConfigFile) {
			Game.currentConfig.put("width", side);
			Game.currentConfig.put("height", side);
			Game.currentConfig.put("human mark", marksArray[0].charAt(0));
			Game.currentConfig.put("goblin mark", marksArray[1].charAt(0));
			Game.currentConfig.put("elf mark", marksArray[2].charAt(0));
			Game.currentConfig.put("orc mark", marksArray[3].charAt(0));
			Game.currentConfig.put("obstacle mark", marksArray[4].charAt(0));
		}
		else {
			Game.currentConfig = Game.defaultConfig;
		}

		// Configuration changed; display the new config
		Game.displayConfig();
		
    }

	/**
	 * Initialize the variable {@link Game#defaultConfig}
	 */
	public static void initializeDefaultConfig() {
		Game.defaultConfig.put("width", Game.DEFAULT_SIDE);
		Game.defaultConfig.put("height", Game.DEFAULT_SIDE);
		Game.defaultConfig.put("human mark", Game.DEFAULT_HUMAN_MARK);
		Game.defaultConfig.put("goblin mark", Game.DEFAULT_GOBLIN_MARK);
		Game.defaultConfig.put("elf mark", Game.DEFAULT_ELF_MARK);
		Game.defaultConfig.put("orc mark", Game.DEFAULT_ORC_MARK);
		Game.defaultConfig.put("obstacle mark", Game.DEFAULT_OBSTACLE_MARK);

	}

	/**
	 * Method called to create an instance of the class Game if that instance does not exist
	 * or else return the existing one
	 *
	 * @return The existing instance of the class {@link Game} if it exist or else create and return a new one
	 */
    static Game getInstance() {
      if (Game.myGame==null) {
    	 Game.myGame = new Game();
      }
      else {
    	  LOGGER.warning("Trying to instantiate the class Game more than one time");
      }
  	  return myGame;
    }

    /**
     * This method start the game and display the intermediate result
     */
    Race[] start() throws IOException, InterruptedException {
    	Race[] ranking = new Race[4];
		Human.setMark((char)Game.currentConfig.get("human mark"));
		Goblin.setMark((char)Game.currentConfig.get("goblin mark"));
		Elf.setMark((char)Game.currentConfig.get("elf mark"));
		Orc.setMark((char)Game.currentConfig.get("orc mark"));
		Obstacle.setMark((char)Game.currentConfig.get("obstacle mark"));
		Collections.shuffle(namesPositionArray);
		Game.map = new Map((int)Game.currentConfig.get("width"), (int)Game.currentConfig.get("height"), namesPositionArray);
		Game.map.initialize();

		ArrayList<Being> beings = (ArrayList<Being>) Game.map.getBeings().clone();
        Game.map.drawMap();
        System.out.println("\n\n");
		boolean finished = false;
		int roundNumber = 1;
		while (!finished) {
            Collections.shuffle(beings);

            for (Being being : beings) {
                if (!Game.map.getBeings().contains(being))
                    continue;
                if (being instanceof Human)
                    ((Human)being).Bout();
                else if (being instanceof Elf)
                    ((Elf)being).Bout();
                else if (being instanceof Orc)
                    ((Orc)being).Bout();
                else if (being instanceof Goblin)
                    ((Goblin)being).Bout();
            }
            beings = (ArrayList<Being>) Game.map.getBeings().clone();
            Integer[] stat = countRemainingBeingsByRace(beings);
            int numberOfRaceRemaining = countRemainingRaces(stat);
            if (numberOfRaceRemaining == 2) {
                Being[] remainingRaces = find2RemainingRaces(beings);
                if (Being.getRace(remainingRaces[0]) == Being.getRace(remainingRaces[1])) {
                    finished = true;
                    ranking[0] = Being.getRace(remainingRaces[0]);
                }
                else if (Being.getType(remainingRaces[0]) == Being.getType(remainingRaces[1])) {
                    finished = true;
                    if (Being.getType(remainingRaces[0]) == Type.KIND) {
                        if (stat[0] > stat[1]) {
                            ranking[0] = Race.HUMAN;
                            ranking[1] = Race.ELF;
                        }
                        else if (stat[0] < stat[1]) {
                            ranking[0] = Race.ELF;
                            ranking[1] = Race.HUMAN;
                        }
                        else {
                            ranking[0] = Race.HUMAN;
                            ranking[1] = Race.ELF;
                        }
                    }
                    else if (Being.getType(remainingRaces[0]) == Type.EVIL) {
                        if (stat[2] > stat[3]) {
                            ranking[0] = Race.GOBLIN;
                            ranking[1] = Race.ORC;
                        }
                        else if (stat[2] < stat[3]) {
                            ranking[0] = Race.ORC;
                            ranking[1] = Race.GOBLIN;
                        }
                        else {
                            ranking[0] = Race.GOBLIN;
                            ranking[1] = Race.ORC;
                        }
                    }
                }
            }
            else if (numberOfRaceRemaining < 2) {
                ranking[0] = Being.getRace(beings.get(0));
                finished = true;
            }
            if ((roundNumber == Game.MAX_ROUND/2) || (roundNumber == Game.MAX_ROUND/4) || (roundNumber == 3 * Game.MAX_ROUND/4)) {
                System.out.println("Round " + roundNumber + ":");
                System.out.println("Number of human: " + stat[0]);
                System.out.println("Number of elf: " + stat[1]);
                System.out.println("Number of goblin: " + stat[2]);
                System.out.println("Number of orc: " + stat[3]);
                System.out.println();
                Thread.sleep(1500);
            }
            if (roundNumber == Game.MAX_ROUND) {
                System.out.println("Round " + roundNumber + ":");
                System.out.println("Number of human: " + stat[0]);
                System.out.println("Number of elf: " + stat[1]);
                System.out.println("Number of goblin: " + stat[2]);
                System.out.println("Number of orc: " + stat[3]);
                System.out.println();
                List<Integer> tmp = Arrays.asList(stat);
                int count = tmp.indexOf(Collections.max(tmp));
                if (count == 0)
                    ranking[0] = Race.HUMAN;
                else if (count == 1)
                    ranking[0] = Race.ELF;
                else if (count == 2)
                    ranking[0] = Race.GOBLIN;
                else if (count == 3)
                    ranking[0] = Race.ORC;
                finished = true;
            }
            roundNumber++;
            Thread.sleep(1000);
        }
		System.out.println("Over game ...\n");
    	return ranking;
    }

	/**
	 * Count the remaining beings for each race and return an array
	 * The first value of the array is the number of human remain
	 * The second value represent the number of elf remaining
	 * The third value represent the number of goblin remaining
	 * The fourth value represent the number of orc remaining
	 *
	 * @param beings the remaining beings
	 * @return An array containing the number of beings remaining for each race
	 */
	public Integer[] countRemainingBeingsByRace(ArrayList<Being> beings) {
        Integer[] tmp = new Integer[4];
        for (int i = 0; i < 4; i++) {
            tmp[i] = 0;
        }
        for (Being being : beings) {
            if (being instanceof Human)
                tmp[0]++;
            else if (being instanceof Elf)
                tmp[1]++;
            else if (being instanceof Goblin)
                tmp[2]++;
            else if (being instanceof Orc)
                tmp[3]++;
        }
        return tmp;
    }

	/**
	 * Return an array containing 2 different being with 2 different races
	 *
	 * @param beings The remaining beings
	 * @return A 2 dimensional array containing 2 beings of 2 different races
	 */
	public Being[] find2RemainingRaces(ArrayList<Being> beings) {
        Being[] tmp = new Being[2];
        for (Being being : beings) {
            if (tmp[0] == null)
                tmp[0] = being;
            else {
                if (((tmp[0] instanceof Kind) && (being instanceof Evil)) || ((tmp[0] instanceof Evil) && (being instanceof Kind)))
                    tmp[1] = being;
            }
        }
        return tmp;
    }

	/**
	 * Count the remaining races using the array of integer returned by the method {@link Game#countRemainingRaces(Integer[])}
	 *
	 * @param stat The array returned by the method {@link Game#countRemainingRaces(Integer[])}
	 * @return The number of remaining races
	 */
	public int countRemainingRaces(Integer[] stat) {
        int tmp = 0;
        for (int i = 0; i < stat.length; i++) {
            if (stat[i] != 0)
                tmp++;
        }
        return tmp;
    }

    /**
     * This method allow the user to change the game default parameters
     */
    static void changeConfig() {
    	Scanner scInt = new Scanner(System.in);
		Scanner scStr = new Scanner(System.in);
    	String tmp;
		int side;
		int round;
		char humanMark;
		char elfMark;
		char goblinMark;
		char orcMark;
		char obstacleMark;

		System.out.println("* Allowed sides: " + String.join(" ", Game.ALLOWED_SIDE));
    	System.out.print("    - width [50]: ");
    	tmp = scInt.nextLine();
    	try {
			side = Integer.parseInt(tmp);
            if(!(side == 50 || side == 100 || side == 250 || side == 500)) {
                side = Game.DEFAULT_SIDE;
            }
		} catch (NumberFormatException e) {
    		side = Game.DEFAULT_SIDE;
		}

        System.out.print("    - max number of round [10]: ");
        tmp = scInt.nextLine();
        try {
            round = Integer.parseInt(tmp);
            if(!(round > 100)) {
                round = 10;
            }
        } catch (NumberFormatException e) {
            round = 10;
        }
        Game.MAX_ROUND = round;

    	System.out.println("* : The marks are the character used to distinguish each race in the map");
		System.out.print("    - Human mark* [h]: ");
		tmp = scStr.nextLine();
		if (tmp.length() == 0) {
			humanMark = Game.DEFAULT_HUMAN_MARK;
		} else {
			humanMark = tmp.charAt(0);
		}
		System.out.print("    - elf mark* [e]: ");
		tmp = scStr.nextLine();
		if (tmp.length() == 0) {
			elfMark = Game.DEFAULT_ELF_MARK;
		} else {
			elfMark = tmp.charAt(0);
		}
		System.out.print("    - orc mark* [o]: ");
		tmp = scStr.nextLine();
		if (tmp.length() == 0) {
			orcMark = Game.DEFAULT_ORC_MARK;
		} else {
			orcMark = tmp.charAt(0);
		}
		System.out.print("    - goblin mark* [g]: ");
		tmp = scStr.nextLine();
		if (tmp.length() == 0) {
			goblinMark = Game.DEFAULT_GOBLIN_MARK;
		} else {
			goblinMark = tmp.charAt(0);
		}
		System.out.print("    - obstacle mark* [#]: ");
		tmp = scStr.nextLine();
		if (tmp.length() == 0) {
			obstacleMark = Game.DEFAULT_OBSTACLE_MARK;
		} else {
			obstacleMark = tmp.charAt(0);
		}
		Game.currentConfig.replace("width", side);
		Game.currentConfig.replace("height", side);
		Game.currentConfig.replace("human mark", humanMark);
		Game.currentConfig.replace("elf mark", elfMark);
		Game.currentConfig.replace("goblin mark", goblinMark);
		Game.currentConfig.replace("orc mark", orcMark);
		Game.currentConfig.replace("obstacle mark", obstacleMark);

    }

    /**
     * Display the config to the player
     */
    static void displayConfig() {
		System.out.println("======================= Current config ========================");
		System.out.print(padRight("    - Side: ", 10));
		System.out.println(Game.currentConfig.get("width"));
		System.out.print(padRight("    - max round: ", 10));
		System.out.println(Game.MAX_ROUND);
		System.out.print(padRight("    - human mark: ", 10));
		System.out.println(Game.currentConfig.get("human mark"));
		System.out.print(padRight("    - orc mark: ", 10));
		System.out.println(Game.currentConfig.get("orc mark"));
		System.out.print(padRight("    - elf mark: ", 10));
		System.out.println(Game.currentConfig.get("elf mark"));
		System.out.print(padRight("    - goblin mark: ", 10));
		System.out.println(Game.currentConfig.get("goblin mark"));
		System.out.print(padRight("    - obstacle mark: ", 10));
		System.out.println(Game.currentConfig.get("obstacle mark"));


    }

	public static String padRight(String s, int n) {
		return String.format("%-" + n + "s", s);
	}

	/**
	 * Read the content on the specific line of the file
	 *
	 * @param line The line to read
	 * @param fileName The file in which to read
	 * @return The value at that line
	 * @throws FileNotFoundException When the file is not found
	 * @throws IOException Error when using the file
	 */
	public static String readLine(int line, String fileName) throws IOException, FileNotFoundException {
		String content;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			for (int i = 0; i < line - 1; i++)
				br.readLine();
			content =  br.readLine();
			br.close();
			return content;
		}
	}
}

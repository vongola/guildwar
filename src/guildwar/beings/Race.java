package guildwar.beings;

/**
 * Define the different Race in the game:
 * <ul>
 * <li>{@link #ELF}</li>
 * <li>{@link #HUMAN}</li>
 * <li>{@link #ORC}</li>
 * <li>{@link #GOBLIN}</li>
 * </ul>
 */
public enum Race {

    /**
     * The human race
     */
    HUMAN("Human"),
    /**
     * The elf race
     */
    ELF("Elf"),
    /**
     * The orc race
     */
    ORC("Orc"),
    /**
     * The goblin race
     */
    GOBLIN("Goblin");

    private String race;

    private Race(String race) {
        this.race = race;
    }

    @Override
    public String toString() {
        return this.race;
    }

}

package guildwar.beings;


import guildwar.beings.evil.Evil;
import guildwar.beings.evil.Goblin;
import guildwar.beings.evil.Orc;
import guildwar.beings.kind.Elf;
import guildwar.beings.kind.Human;
import guildwar.beings.kind.Kind;
import guildwar.map.Direction;
import guildwar.map.Point;
import guildwar.map.Obstacle;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Define a being
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public abstract class Being {

    /**
     * Health point of the being
     */
    protected int hp;
    /**
     * Strength point of the being
     */
    protected int sp;
    /**
     * Experience point of the being
     */
    protected int xp;
    /**
     * Attack point of the being
     */
    protected int ap;
    /**
     * Defense point of the being
     */
    protected int dp;
    /**
     * The last direction the being chose
     */
    protected Direction lastDirection;
    /**
     * The name of the being
     */
    protected String name;
    /**
     * The current point of the being
     */
    protected Point currentPoint;

    private Random r = new Random();
    /**
     * An array representing each directions
     */
    protected Direction[] directions = {Direction.EAST, Direction.NORTH, Direction.NORTHEAST, Direction.NORTHWEST, Direction.SOUTH, Direction.SOUTHEAST, Direction.SOUTHWEST, Direction.WEST};
    /**
     * An array representing the directions to use to go to the safeZone
     */
    protected Direction[] safeDirection = new Direction[3];
    /**
     * The max number of step we can make
     */
    protected static int maxStep;
    /**
     * Is equal to true if it is the first time the being is moving
     */
    protected boolean firstMove;

    /**
     * The constructor
     *
     * @param currentPoint The current point if the being
     * @param name The name of the being
     */
    public Being(Point currentPoint, String name) {
        this.name = name;
        this.currentPoint = currentPoint;
        this.firstMove = true;
        this.setMaxStep();
    }

    /**
     * The Getter for the attribute {@link #hp}
     * @return {@link #hp}
     */
    public int getHp() {
        return this.hp;
    }

    public abstract char getMark();

    /**
     * The Getter for the attribute {@link #name}
     * @return {@link #name}
     */
    public String getName() { return this.name; }

    /**
     * The Getter for the attribute {@link #sp}
     * @return {@link #sp}
     */
    public int getSp() {
        return this.sp;
    }

    /**
     * The Getter for the attribute {@link #xp}
     * @return {@link #xp}
     */
    public int getXp() {
        return this.xp;
    }

    /**
     * The Getter for the attribute {@link #ap}
     * @return {@link #ap}
     */
    public int getAp() {
        return this.ap;
    }

    /**
     * The Setter for the attribute {@link #hp}
     */
    public void setHp(int newHp) { this.hp = newHp; }

    /**
     * The Setter for the attribute {@link #sp}
     */
    public void setSp(int newSp) { this.sp = newSp; }

    /**
     * The Setter for the attribute {@link #xp}
     */
    public void setXp(int newXp) { this.xp = newXp; }

    /**
     * The Setter for the attribute {@link #ap}
     */
    public void setAp(int newAp) { this.ap = newAp; }

    /**
     * The Getter for the attribute {@link #currentPoint}
     * @return {@link #currentPoint}
     */
    public Point getCurrentPoint() {
        return this.currentPoint;
    }

    /**
     * The Setter for the attribute {@link #currentPoint}
     */
    public void setCurrentPoint(Point newPoint) {
        this.currentPoint = newPoint;
    }

    /**
     * The Getter for the attribute {@link #lastDirection}
     * @return {@link #lastDirection}
     */
    public Direction getLastDirection() {
        return this.lastDirection;
    }

    /**
     * Set the variable {@link #safeDirection}
     * @param race The race of the being
     */
    protected void setSafeDirection(Race race) {
        int width = currentPoint.map.getWidth();
        int height = currentPoint.map.getHeight();
        Race safe00 = currentPoint.map.getSafeZones().get(0).getRace();
        Race safe11 = currentPoint.map.getSafeZones().get(3).getRace();
        Race safe01 = currentPoint.map.getSafeZones().get(1).getRace();
        Race safe10 = currentPoint.map.getSafeZones().get(2).getRace();
        
        if(race == safe00) {
            safeDirection[0] = Direction.NORTHWEST;
            safeDirection[1] = Direction.NORTH;
            safeDirection[2] = Direction.WEST;
        }else if(race == safe10) {
            safeDirection[0] = Direction.NORTHEAST;
            safeDirection[1] = Direction.NORTH;
            safeDirection[2] = Direction.EAST;
        }else if(race == safe01) {
            safeDirection[0] = Direction.SOUTHWEST;
            safeDirection[1] = Direction.SOUTH;
            safeDirection[2] = Direction.WEST;
        }else if(race == safe11) {
            safeDirection[0] = Direction.SOUTHEAST;
            safeDirection[1] = Direction.SOUTH;
            safeDirection[2] = Direction.EAST;
        }
    }

    /**
     * Set the max number of step we can have in one round for a being
     */
    private void setMaxStep() {
        int width = currentPoint.map.getWidth();
        int height = currentPoint.map.getHeight();
        Being.maxStep = (int)(Math.min(width, height) / 8);
    }

    /**
     * Move the being from a position to another
     *
     * @param x_next The next x position of the being
     * @param y_next The next y position of the being
     * @param step The step remaining
     * @param race The race of the being
     * @return the remaining step after the move
     */
    public int move(int x_next,int y_next, int step, Race race) {

        if(this.currentPoint.map.getPointMapping()[x_next][y_next].isSafeZone() && this.currentPoint.map.getPointMapping()[x_next][y_next].getSafeZone().getRace() == race) {  //next one is the safe zone
            this.sp = Math.min(this.sp + 3, Being.getMaxStat(this).getMaxSP());
        }
        else {
            if (this.sp > 0)
                this.sp = this.sp - 1;
        }
        if (this.hp + 1 <= Being.getMaxStat(this).getMaxHP())
            this.hp = this.hp + 1;

        this.currentPoint.getContent().setValueToNull();
        this.currentPoint = this.currentPoint.map.getPointMapping()[x_next][y_next];
        this.currentPoint.getContent().setValue(this);

        step--;
        return step;
    }

    /**
     * The being will stay in his previous position
     *
     * @param step The number of step
     * @param race The race of the being
     * @return The number of step remaining
     */
    private int stay(int step, Race race) {
        if (!(this.currentPoint.isSafeZone()) || this.currentPoint.getSafeZone().getRace() == race) {
            if (this.sp - step >= 0)
                this.sp -= step;
            else
                this.sp = 0;
        }
        return 0;
    }

    /**
     * This function is called to try moving a being
     * Depending on the situation, it will call {@link #move(int, int, int, Race)} , {@link #stay(int, Race)} or {@link #fight(Object)}
     * @param race The race of the being
     * @param maxStat Tne maxStat of the being depending on the type of being
     */
    public void Bout(Race race, MaxStat maxStat) {
        int meetReturn;
        int backStep;
        int directionNum;
        Direction dir;
        String direction;
        int step;
        Point nextCase;
        int x, x_next = currentPoint.getX();
        int y, y_next = currentPoint.getY();
        int moveSP;

        if (this instanceof Kind) {
            moveSP = Kind.getMaxStat().getMaxSP() * 20 / 100;
        }
        else if (this instanceof Evil) {
            moveSP = Evil.getMaxStat().getMaxSP() * 20 / 100;
        }
        else {
            moveSP = 10;
        }
        if (this.sp == 0) {
            return;
        }
        else if(this.sp >= moveSP ) {
            List<Integer> dirList;
            if (this.currentPoint.getX() == 0 && this.currentPoint.getY() == 0) {
                dirList = Arrays.asList(0, 4, 5);
                directionNum = dirList.get(r.nextInt(dirList.size()));
                dir = this.directions[directionNum];
            }
            else if (this.currentPoint.getX() == this.currentPoint.map.getWidth() && this.currentPoint.getY() == 0) {
                dirList = Arrays.asList(4, 6, 7);
                directionNum = dirList.get(r.nextInt(dirList.size()));
                dir = this.directions[directionNum];
            }
            else if (this.currentPoint.getX() == this.currentPoint.map.getWidth() && this.currentPoint.getY() == this.currentPoint.map.getHeight()) {
                dirList = Arrays.asList(1, 3, 7);
                directionNum = dirList.get(r.nextInt(dirList.size()));
                dir = this.directions[directionNum];
            }
            else if (this.currentPoint.getY() == this.currentPoint.map.getHeight() && this.currentPoint.getX() == 0) {
                dirList = Arrays.asList(0, 1, 2);
                directionNum = dirList.get(r.nextInt(dirList.size()));
                dir = this.directions[directionNum];
            }
            else {
                if(firstMove) {
                    directionNum = r.nextInt(8);
                    dir = this.directions[directionNum];
                }
                else {
                    do {
                        directionNum = r.nextInt(8);
                        dir = this.directions[directionNum];

                    }while(dir == this.lastDirection);
                }
            }
            backStep = Math.min(Being.maxStep, this.sp);
            step = 1 + r.nextInt(Being.maxStep);

            direction = this.directions[directionNum].toString();
            this.lastDirection = this.directions[directionNum];
        }
        else {
            if(firstMove) {
                directionNum = r.nextInt(3);
                dir = this.directions[directionNum];
            }
            else {
                do {
                    directionNum = r.nextInt(3);
                    dir = safeDirection[directionNum];

                }while(dir == this.lastDirection);
            }
            backStep = Math.min(Being.maxStep, this.sp);
            step = 1 + r.nextInt(backStep);

            direction = safeDirection[directionNum].toString();
            this.lastDirection = safeDirection[directionNum];
            this.firstMove = false;
        }
        do {
            if (this.sp == 0) {
                return;
            }

            if(direction.toLowerCase().contains("north")) y_next++;
            if(direction.toLowerCase().contains("south")) y_next--;
            if(direction.toLowerCase().contains("west")) x_next--;
            if(direction.toLowerCase().contains("east")) x_next++;

            //nextCase =new Point(x_next, y_next);
            try {
                nextCase = this.currentPoint.map.getPointMapping()[x_next][y_next];
            } catch (IndexOutOfBoundsException e) {
                stay(step, race);
                break;
            }
            Object obj = nextCase.getContent().getValue();
            if(obj==null) {   //empty case
                step = move(x_next, y_next, step, race);
            }
            else if (obj instanceof Obstacle) {
                step = stay(step, race);
            }
            else {
                meetReturn = meet(obj);
                switch (meetReturn) {
                    case 0:
                        int resultFight = fight(obj);
                        switch (resultFight) {
                            case 0:
                                step = move(x_next, y_next, step, race);
                                this.currentPoint.map.getBeings().remove(obj);
                                break;
                            case 1:
                                this.currentPoint.getContent().setValueToNull();  //delete being
                                this.currentPoint.map.getBeings().remove(this);
                                return;
                            case 2:
                                if(step == 1) {
                                    step = stay(step, race);
                                }
                                break;
                            default:
                        }
                        break;
                    case 1:
                        this.hp = Math.min(Being.getMaxStat(this).getMaxHP(), this.hp + step/2);
                        ((Being) obj).hp = Math.min(Being.getMaxStat(obj).getMaxHP(), ((Being) obj).hp + step/2);
                        break;
                    case 2:
                        this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + step/2);
                        ((Being) obj).xp = Math.min(Being.getMaxStat(obj).getMaxXP(), ((Being) obj).xp + step/2);
                        break;
                    default:
                }
            }

        }while(step > 0);
    }

    /**
     * The being will fight an opponent
     *
     * @param op The opponent
     * @return 0 if the opponent died, 1 if the being died and 2 if no one died
     */
    private int fight(Object op) {
        Being opponent = (Being)op;
        int result;
        int counter;
        int attack = this.xp + r.nextInt(10) + this.ap - 5;
        int defense =  opponent.xp + r.nextInt(10) + opponent.dp - 5;
        result = (attack) - (defense);
        if (opponent.sp == 0) {
            opponent.hp = 0;
            this.ap = Math.min(Being.getMaxStat(this).getMaxAP(), this.ap + Math.abs(result));
            this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + 3);
            return 0;
        }
        if (result < 0) {
            opponent.dp = Math.min(Being.getMaxStat(opponent).getMaxDP(), opponent.dp + Math.abs(result));
            this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + 1);
            opponent.xp = Math.min(Being.getMaxStat(opponent).getMaxXP(), opponent.xp + 2);
        }
        else if (result == 0) {
            opponent.dp = Math.min(Being.getMaxStat(opponent).getMaxDP(), opponent.dp + 1);
            this.ap = Math.min(Being.getMaxStat(this).getMaxAP(), this.ap + 1);
            this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + 1);
            opponent.xp = Math.min(Being.getMaxStat(opponent).getMaxXP(), opponent.xp + 1);
        }
        else {
            opponent.hp = Math.max(0, opponent.hp - Math.abs(result) - r.nextInt(5));
            this.ap = Math.min(Being.getMaxStat(this).getMaxAP(), this.ap + Math.abs(result));
            this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + 3);
            // check that the enemy has enough hp
            if (opponent.hp == 0) {
                return 0;
            }
        }
        // The enemy is attacking
        attack = opponent.xp + r.nextInt(10) + opponent.ap - 5;
        defense = this.xp + r.nextInt(10) + this.dp - 5;
        counter = (attack) - (defense);
        if (this.sp == 0) {
            this.hp = 0;
            opponent.ap = Math.min(Being.getMaxStat(opponent).getMaxAP(), opponent.ap + Math.abs(counter));
            opponent.xp = Math.min(Being.getMaxStat(opponent).getMaxXP(), opponent.xp + 3);
            return 1;
        }
        if (counter < 0) {
            this.dp = Math.min(Being.getMaxStat(this).getMaxDP(), this.dp + Math.abs(counter));
            this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + 2);
            opponent.xp = Math.min(Being.getMaxStat(opponent).getMaxXP(), opponent.xp + 1);
        }
        else if (counter == 0) {
            this.dp = Math.min(Being.getMaxStat(this).getMaxDP(), this.dp + 1);
            this.ap = Math.min(Being.getMaxStat(opponent).getMaxAP(), opponent.ap + 1);
            this.xp = Math.min(Being.getMaxStat(this).getMaxXP(), this.xp + 1);
            opponent.xp = Math.min(Being.getMaxStat(opponent).getMaxXP(), opponent.xp + 1);
        }
        else {
            this.hp = Math.max(0, this.hp - Math.abs(counter) - r.nextInt(5));
            opponent.ap = Math.min(Being.getMaxStat(opponent).getMaxAP(), opponent.ap + Math.abs(counter));
            opponent.xp = Math.min(Being.getMaxStat(opponent).getMaxXP(), opponent.xp + 3);
            // check that the enemy has enough hp
            if (this.hp == 0) {
                return 1;
            }
        }
        return 2;
    }

    /**
     * Meet another being
     *
     * @param obj The other being
     * @return 0 if it is an enemy, 1 if it is someone of the same race and 1 if it is someone of the same type but not the same race
     */
    private int meet(Object obj) {
        if (((this instanceof Kind) && (obj instanceof Evil)) || ((this instanceof Evil) && (obj instanceof Kind))) {
            return 0;
        }
        else if (((this instanceof Kind) && (obj instanceof Kind)) || ((this instanceof Evil) && (obj instanceof Evil))) {
            if (this.getClass() == obj.getClass()) {
                return 1;
            }
            else {
                return 2;
            }
        }
        else {
            return -1;
        }
    }

    /**
     * Get the maxStat of the being passed as parameter
     *
     * @param obj The being we want to get the maxStat
     * @return {@link MaxStat}
     */
    private static MaxStat getMaxStat(Object obj) {
        if (obj instanceof Evil)
            return Evil.getMaxStat();
        else if (obj instanceof Kind)
            return Kind.getMaxStat();
        else
            return null;
    }

    /**
     * Get the race of the being passed as parameter
     *
     * @param obj The being we want to know the race
     * @return {@link Race}
     */
    public static Race getRace(Object obj) {
        if (obj instanceof Elf)
            return Race.ELF;
        else if (obj instanceof Human)
            return Race.HUMAN;
        else if (obj instanceof Orc)
            return Race.ORC;
        else if (obj instanceof Goblin)
            return Race.GOBLIN;
        else
            return null;
    }

    /**
     * Get the type of the being passed as parameter
     *
     * @param obj The being we want to know the type
     * @return {@link Type}
     */
    public static Type getType(Object obj) {
        if (obj instanceof Kind)
            return Type.KIND;
        else if (obj instanceof Evil)
            return Type.EVIL;
        else
            return null;
    }

}
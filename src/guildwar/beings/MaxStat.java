package guildwar.beings;

import java.util.HashMap;

/**
 * Define the max of each stat (hp, xp, ap, dp and sp) for a type of being
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class MaxStat {
    /**
     * An {@link HashMap} with each stat and their value
     */
    HashMap<String, Integer> max = new HashMap<>(5);

    public MaxStat(int hp, int xp, int sp, int ap, int dp) {
        this.max.put("hp", hp);
        this.max.put("sp", sp);
        this.max.put("ap", ap);
        this.max.put("xp", xp);
        this.max.put("dp", dp);
    }

    /**
     * Get the max of the stat HP
     * @return The max of hp
     */
    public int getMaxHP() { return this.max.get("hp"); }

    /**
     * Get the max of the stat XP
     * @return The max of xp
     */
    public int getMaxXP() { return this.max.get("xp"); }

    /**
     * Get the max of the stat SP
     * @return The max of sp
     */
    public int getMaxSP() {
        return this.max.get("sp");
    }

    /**
     * Get the max of the stat AP
     * @return The max of ap
     */
    public int getMaxAP() {
        return this.max.get("ap");
    }

    /**
     * Get the max of the stat DP
     * @return The max of dp
     */
    public int getMaxDP() { return this.max.get("dp"); }

    /**
     * Set the max of each stat
     */
    public void setStat(int hp, int xp, int sp, int ap, int dp) {
        this.max.replace("hp", hp);
        this.max.replace("sp", sp);
        this.max.replace("ap", ap);
        this.max.replace("xp", xp);
        this.max.replace("dp", dp);
    }
}
package guildwar.beings;

/**
 * The different types of beings
 * <ul>
 * <li>{@link #EVIL}</li>
 * <li>{@link #KIND}</li>
 * </ul>
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public enum Type {

    /**
     * EVIL
     */
    EVIL("Evil"),
    /**
     * KIND
     */
    KIND("Kind");

    private String type;

    private Type(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return this.type;
    }
}


package guildwar.beings.evil;

import guildwar.beings.Race;
import guildwar.map.Point;

/**
 * Define an Orc
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class Orc extends Evil {

    /**
     * The race of the being
     */
    private Race race = Race.ORC;
    /**
     * The mark for the orc
     */
    private static char mark ;

    /**
     * Constructor.
     * Set attribute specific to orc
     */
    public Orc(Point currentPoint, String name) {
        super(currentPoint, name);
        this.hp = 50;
        this.xp = 46;
        this.ap = 10;
        this.sp = 500;
        this.dp = 10;
        this.setSafeDirection(this.race);
    }

    /**
     * The getter for the attribute {@link #mark}
     *
     * @return {@link #mark}
     */
    public char getMark() {
    	return Orc.mark;
    }

    /**
     * The setter for the attribute {@link #mark}
     *
     */
    public static void setMark(char newMark) {
        Orc.mark = newMark;
    }

    /**
     * This function is called to try moving a being
     * Will call the method of the same name in the super class
     *
     */
    public void Bout() {
        super.Bout(this.race);
    }

    /**
     * The getter for the attribute {@link #race}
     *
     * @return {@link #race}
     */
    public Race getRace() {
        return this.race;
    }

    @Override
    public String toString() {
        return "[ " + this.name + " - " + this.race + " ]";
    }
}

package guildwar.beings.evil;
import guildwar.beings.Being;
import guildwar.beings.MaxStat;
import guildwar.beings.Type;
import guildwar.map.Point;
import guildwar.beings.Race;

/**
 * Define a being of type Evil
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public abstract class Evil extends Being {
    /**
     * The type of the being
     */
    private Type type = Type.EVIL;
    /**
     * The maxStat of all the being of the type Evil
     */
    protected static MaxStat maxStat = new MaxStat(100, 100, 1100, 150, 110);

    /**
     * Constructor.
     * Set Max HP, ...
     */
    public Evil(Point currentPoint, String name) {
        super(currentPoint, name);
        if (currentPoint.map.getWidth() == 50)
            Evil.maxStat.setStat(100, 100, 520, 110, 105);
        else if (currentPoint.map.getWidth() == 100)
            Evil.maxStat.setStat(100, 150, 1100, 110, 105);
        else if (currentPoint.map.getWidth() == 250)
            Evil.maxStat.setStat(100, 150, 2600, 110, 105);
        else
            Evil.maxStat.setStat(100, 150, 5500, 110, 105);
    }

    /**
     * Getter of the attribute {@link #type}
     * @return {@link #type}
     */
    public Type getType() {
        return this.type;
    }

    /**
     * This function is called to try moving a being
     * Will call the method of the same name in the super class
     *
     * @param race The race of the being
     */
    public void Bout(Race race) {
        super.Bout(race, Evil.maxStat);
    }

    /**
     * Getter of the attribute {@link #maxStat}
     * @return {@link #maxStat}
     */
    public static MaxStat getMaxStat() {
        return Evil.maxStat;
    }
}


package guildwar.beings.kind;

import guildwar.beings.Race;
import guildwar.map.Point;

/**
 * Define a human
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class Human extends Kind {

    /**
     * The race of the being
     */
    private Race race = Race.HUMAN;
    /**
     * The mark for the humans
     */
    private static char mark;

    /**
     * Constructor.
     * Set attribute specific to human
     */
    public Human(Point currentPoint, String name) {
        super(currentPoint, name);
        this.hp = 50;
        this.xp = 50;
        this.ap = 8;
        this.sp = 500;
        this.dp = 9;
        this.setSafeDirection(this.race);
    }

    /**
     * The getter for the attribute {@link #mark}
     *
     * @return {@link #mark}
     */
    public char getMark() {
    	return Human.mark;
    }

    /**
     * The setter for the attribute {@link #mark}
     *
     */
    public static void setMark(char newMark) {
        Human.mark = newMark;
    }

    /**
     * This function is called to try moving a being
     * Will call the method of the same name in the super class
     *
     */
    public void Bout() {
        super.Bout(this.race);
    }

    /**
     * The getter for the attribute {@link #race}
     *
     * @return {@link #race}
     */
    public Race getRace() {
        return this.race;
    }

    @Override
    public String toString() {
        return "[ " + this.name + " - " + this.race + " ]";
    }
}

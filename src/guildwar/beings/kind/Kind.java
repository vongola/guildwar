

package guildwar.beings.kind;
import guildwar.beings.Being;
import guildwar.beings.MaxStat;
import guildwar.beings.Type;
import guildwar.map.Point;
import guildwar.beings.Race;

/**
 * Define a being of type Kind
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public abstract class Kind extends Being {
    /**
     * The type of the being
     */
    private Type type = Type.KIND;
    /**
     * The maxStat of all the being of the type Kind
     */
    protected static MaxStat maxStat = new MaxStat(110, 150, 1000, 100, 100);

    /**
     * Constructor.
     * Set Max HP, ...
     */
    public Kind(Point currentPoint, String name ) {
        super(currentPoint, name);
        if (currentPoint.map.getWidth() == 50)
            Kind.maxStat.setStat(110, 150, 500, 100, 100);
        else if (currentPoint.map.getWidth() == 100)
            Kind.maxStat.setStat(110, 150, 1000, 100, 100);
        else if (currentPoint.map.getWidth() == 250)
            Kind.maxStat.setStat(110, 150, 2500, 100, 100);
        else
            Kind.maxStat.setStat(110, 150, 5000, 100, 100);
    }

    /**
     * Getter of the attribute {@link #type}
     * @return {@link #type}
     */
    public Type getType() {
        return this.type;
    }

    /**
     * This function is called to try moving a being
     * Will call the method of the same name in the super class
     *
     * @param race The race of the being
     */
    public void Bout(Race race) {
        super.Bout(race, Kind.maxStat);
    }

    /**
     * Getter of the attribute {@link #maxStat}
     * @return {@link #maxStat}
     */
    public static MaxStat getMaxStat() {
        return Kind.maxStat;
    }
}


package guildwar.beings.kind;

import guildwar.beings.Race;
import guildwar.map.Point;

/**
 * Define an elf
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class Elf extends Kind {

    /**
     * The race of the being
     */
    private Race race = Race.ELF;
    /**
     * The mark for the elf
     */
    private static char mark;

    /**
     * Constructor.
     * Set attribute specific to elf
     */
    public Elf(Point currentPoint, String name) {
        super(currentPoint, name);
        this.hp = 50;
        this.xp = 50;
        this.ap = 7;
        this.sp = 500;
        this.dp = 8;
        this.setSafeDirection(this.race);
    }

    /**
     * The getter for the attribute {@link #mark}
     *
     * @return {@link #mark}
     */
    public char getMark() {
    	return Elf.mark;
    }

    /**
     * The setter for the attribute {@link #mark}
     *
     */
    public static void setMark(char newMark) {
        Elf.mark = newMark;
    }

    /**
     * This function is called to try moving a being
     * Will call the method of the same name in the super class
     *
     */
    public void Bout() {
        super.Bout(this.race);
    }

    /**
     * The getter for the attribute {@link #race}
     *
     * @return {@link #race}
     */
    public Race getRace() {
        return this.race;
    }

    @Override
    public String toString() {
        return "[ " + this.name + " - " + this.race + " ]";
    }
}

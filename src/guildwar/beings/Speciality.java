package guildwar.beings;

/**
 * !!! Not implemented yet
 * Define the different specialities a being can have. He can have just one speciality
 *
 * <ul>
 * <li>{@link #WARRIOR}</li>
 * <li>{@link #ARCHER}</li>
 * <li>{@link #MAGUS}</li>
 * <li>{@link #ASSASSIN}</li>
 * <li>{@link #SUMMONER}</li>
 * </ul>
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public enum Speciality {

    WARRIOR("Warrior"),
    ARCHER("Archer"),
    MAGUS("Magus"),
    ASSASSIN("Assassin"),
    SUMMONER("Summoner");

    private String speciality;

    private Speciality(String speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {
        return this.speciality;
    }

}

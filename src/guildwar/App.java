package guildwar;

import guildwar.beings.Race;
import guildwar.map.Map;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Class App.java
 * This class contains the main method
 *
 * @author lahat Dème
 * @author Brice Nicodem
 * @author Junior Tagne
 * @author Wanqi Zheng
 */
public class App {

    private static final Logger LOGGER = Logger.getLogger("logger");

    /**
     * The main method of our program
     *
     * @param args The args of the CLI
     */
    public static void main(String[] args) {

        // Initialize and display default config to the player
        Game.initializeDefaultConfig();

        // Get an instance of the class Game
    	Game game = Game.getInstance();

        // Ask the player if he want to change the configs
        System.out.print("[**] Would you like to change this settings ? yes/No/y/N");
        Scanner sc = new Scanner(System.in);
        String request = sc.nextLine();
        if(request.toLowerCase().compareTo("yes") == 0 || request.toLowerCase().compareTo("y") == 0) {
            // If he wants to change the configs, call the function to change the configs
            Game.changeConfig();
        }

    	// Start the game and recover the ranking of each race in an array. The array is ordered by ranking
        System.out.println("Starting the program ...");
    	try {
            Race[] ranking = game.start();
            System.out.println("Winner : " + ranking[0]);
            if (ranking[1] != null) {
                System.out.println("2nd place : " + ranking[1]);
            }
            System.out.println();
        }
    	catch ( IOException ex ) {
            System.out.println("Errors in file's opening");
        } catch (InterruptedException e) {
    	    System.out.println("Interrupting the program ...");
    	    System.exit(0);
        }

        // Display the ranking

        // End the program (System.out.println("End of the program ..."))
        System.out.println("End of the program");

    }

	
}
